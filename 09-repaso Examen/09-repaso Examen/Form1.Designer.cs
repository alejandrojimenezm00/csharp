﻿namespace _09_repaso_Examen
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPalabra = new System.Windows.Forms.TextBox();
            this.textBoxRotacion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPalabra = new System.Windows.Forms.Label();
            this.buttonRotar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Palabra";
            // 
            // textBoxPalabra
            // 
            this.textBoxPalabra.Location = new System.Drawing.Point(66, 13);
            this.textBoxPalabra.Name = "textBoxPalabra";
            this.textBoxPalabra.Size = new System.Drawing.Size(61, 20);
            this.textBoxPalabra.TabIndex = 1;
            // 
            // textBoxRotacion
            // 
            this.textBoxRotacion.Location = new System.Drawing.Point(66, 46);
            this.textBoxRotacion.Name = "textBoxRotacion";
            this.textBoxRotacion.Size = new System.Drawing.Size(60, 20);
            this.textBoxRotacion.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rotacion";
            // 
            // labelPalabra
            // 
            this.labelPalabra.AutoSize = true;
            this.labelPalabra.Location = new System.Drawing.Point(63, 78);
            this.labelPalabra.Name = "labelPalabra";
            this.labelPalabra.Size = new System.Drawing.Size(43, 13);
            this.labelPalabra.TabIndex = 4;
            this.labelPalabra.Text = "Palabra";
            // 
            // buttonRotar
            // 
            this.buttonRotar.Location = new System.Drawing.Point(143, 13);
            this.buttonRotar.Name = "buttonRotar";
            this.buttonRotar.Size = new System.Drawing.Size(59, 19);
            this.buttonRotar.TabIndex = 5;
            this.buttonRotar.Text = "Rotar";
            this.buttonRotar.UseVisualStyleBackColor = true;
            this.buttonRotar.Click += new System.EventHandler(this.buttonRotar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.buttonRotar);
            this.Controls.Add(this.labelPalabra);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxRotacion);
            this.Controls.Add(this.textBoxPalabra);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPalabra;
        private System.Windows.Forms.TextBox textBoxRotacion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPalabra;
        private System.Windows.Forms.Button buttonRotar;
    }
}

