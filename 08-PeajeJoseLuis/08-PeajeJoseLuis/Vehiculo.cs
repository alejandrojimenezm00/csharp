﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_PeajeJoseLuis
{
    class Vehiculo
    {
        private string tipo;
        private string matricula;
        private int ocupantes;


        public Vehiculo (string t, string m, int o)
        {
            tipo = t;
            matricula = m;
            ocupantes = o;
        }

        public Vehiculo ()
        {
            tipo = "turismo";
            matricula = "";
            ocupantes = 0;
        }

        public string devolverTipo()
        {
            return tipo;
        }

        public int devolverOcupantes()
        {
            return ocupantes;
        }

        public string imprimirInfoVehiculo()
        {
            return tipo + " " + matricula + " " + ocupantes;
        }

    }
}
