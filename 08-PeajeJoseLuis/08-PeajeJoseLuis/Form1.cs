﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08_PeajeJoseLuis
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBoxTipo_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxMatricula_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxOcupantes_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPeaje_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelTotalRecaudado_Click(object sender, EventArgs e)
        {

        }

        private void labelTotalOcupantesRegistrados_Click(object sender, EventArgs e)
        {

        }

        private void labelVehiculosRegistrados_Click(object sender, EventArgs e)
        {

        }

        private void labelVehiculoMasOcupado_Click(object sender, EventArgs e)
        {

        }

        private void buttonInsertarVehiculo_Click(object sender, EventArgs e)
        {
            string tipoVehiculo = textBoxTipo.Text;
            string matricula = textBoxMatricula.Text;
            int ocupantes = Int32.Parse(textBoxOcupantes.Text);
            int peaje = Int32.Parse(textBoxPeaje.Text);
            int dineroTotal = 0;
            int ocupantesTotal = 0;

            Vehiculo v = new Vehiculo(tipoVehiculo, matricula, ocupantes);

            Autopista a = new Autopista();

            a.InsertarVehiculoEnPeaje(peaje, v);
            labelImprimirVehiculo.Text = v.imprimirInfoVehiculo();

            for (int i = 0; i < 4; i++)
                dineroTotal += a.CantidadDineroRecaudado(i);
            labelTotalRecaudado.Text = Convert.ToString(dineroTotal);

            for (int i = 0; i < 4; i++)
                ocupantesTotal += a.CantidadOcupantesRegistrados(i);
            labelTotalOcupantesRegistrados.Text = Convert.ToString(ocupantesTotal);

        }

    }
}
