﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_PeajeJoseLuis
{
    class Autopista
    {
        private Peaje[] tabla;

        public Autopista()
        {
            tabla = new Peaje[4];

            for (int i = 0; i < tabla.Length; i++)
                tabla[i] = new Peaje();

        }

        public void InsertarVehiculoEnPeaje(int peaje, Vehiculo v)
        {
            tabla[peaje].InsertarVehiculo(v);
        }

        public int CantidadVehiculos(int peaje)
        {
            return tabla[peaje].CantidadVehiculo();
        }

        public int CantidadDineroRecaudado(int peaje)
        {
            return tabla[peaje].CantidadDineroRecaudado();
        }

        public int CantidadOcupantesRegistrados(int peaje)
        {
            return tabla[peaje].CantidadOcuantesRegistrados();
        }

        public int cantidadOcupantesRegistradosAutopista()
        {
            int ocupantesTotal = 0;

            for (int i = 0; i < tabla.Length; i++)
                ocupantesTotal += tabla[i].CantidadOcuantesRegistrados();

            return ocupantesTotal;
        }

        public Vehiculo maxOcupantes()
        {
            Vehiculo vehiculoMasOcupado = new Vehiculo();

            for (int i = 0; i < tabla.Length; i++)
                if (tabla[i].calcularMaxOcupantes().devolverOcupantes() > vehiculoMasOcupado.devolverOcupantes())
                    vehiculoMasOcupado = tabla[i].calcularMaxOcupantes();

            return vehiculoMasOcupado;
        }


    }
}
