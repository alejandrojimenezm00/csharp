﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_PeajeJoseLuis
{
    class Peaje
    {

        private Vehiculo[] tabla;
        private int nActual;
        private int dineroRecaudado;
        private int ocupantesTotal;

        public Peaje()
        {
            tabla = new Vehiculo[20];
            dineroRecaudado = 0;
            ocupantesTotal = 0;
            nActual = 0;
        }


        public void InsertarVehiculo(Vehiculo v)
        {
        
            switch (v.devolverTipo())
            {
                case "camion":    dineroRecaudado += 20;
                                  break;
                case "turismo":   dineroRecaudado += 15;
                                  break;
                case "colectivo": dineroRecaudado += 10;
                                  break;
            }

            ocupantesTotal += v.devolverOcupantes();

            if (nActual == 20)
            {
                tabla[nActual] = v;
                nActual++;
            }
            
                
        }

        public int CantidadVehiculo()
        {
            return nActual;
        }

        public int CantidadDineroRecaudado()
        {
            return dineroRecaudado;
        }

        public int CantidadOcuantesRegistrados()
        {
            return ocupantesTotal;
        }

        public Vehiculo calcularMaxOcupantes ()
        {
            Vehiculo vehiculoMasOcupadoPeajeI = new Vehiculo();

            for (int i = 0; i < tabla.Length; i++)
                if (tabla[i].devolverOcupantes() > vehiculoMasOcupadoPeajeI.devolverOcupantes())
                    vehiculoMasOcupadoPeajeI = tabla[i];

            return vehiculoMasOcupadoPeajeI;
        }



    }
}
