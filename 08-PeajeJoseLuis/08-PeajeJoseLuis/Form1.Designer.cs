﻿namespace _08_PeajeJoseLuis
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxTipo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMatricula = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxOcupantes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPeaje = new System.Windows.Forms.TextBox();
            this.buttonInsertarVehiculo = new System.Windows.Forms.Button();
            this.labelImprimirVehiculo = new System.Windows.Forms.Label();
            this.labelTotalRecaudado = new System.Windows.Forms.Label();
            this.labelTotalOcupantesRegistrados = new System.Windows.Forms.Label();
            this.labelVehiculosRegistrados = new System.Windows.Forms.Label();
            this.labelVehiculoMasOcupado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxTipo
            // 
            this.textBoxTipo.Location = new System.Drawing.Point(87, 36);
            this.textBoxTipo.Name = "textBoxTipo";
            this.textBoxTipo.Size = new System.Drawing.Size(109, 20);
            this.textBoxTipo.TabIndex = 0;
            this.textBoxTipo.TextChanged += new System.EventHandler(this.textBoxTipo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tipo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Matricula";
            // 
            // textBoxMatricula
            // 
            this.textBoxMatricula.Location = new System.Drawing.Point(87, 62);
            this.textBoxMatricula.Name = "textBoxMatricula";
            this.textBoxMatricula.Size = new System.Drawing.Size(109, 20);
            this.textBoxMatricula.TabIndex = 2;
            this.textBoxMatricula.TextChanged += new System.EventHandler(this.textBoxMatricula_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Ocupantes";
            // 
            // textBoxOcupantes
            // 
            this.textBoxOcupantes.Location = new System.Drawing.Point(87, 88);
            this.textBoxOcupantes.Name = "textBoxOcupantes";
            this.textBoxOcupantes.Size = new System.Drawing.Size(109, 20);
            this.textBoxOcupantes.TabIndex = 4;
            this.textBoxOcupantes.TextChanged += new System.EventHandler(this.textBoxOcupantes_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Peaje";
            // 
            // textBoxPeaje
            // 
            this.textBoxPeaje.Location = new System.Drawing.Point(87, 113);
            this.textBoxPeaje.Name = "textBoxPeaje";
            this.textBoxPeaje.Size = new System.Drawing.Size(109, 20);
            this.textBoxPeaje.TabIndex = 6;
            this.textBoxPeaje.TextChanged += new System.EventHandler(this.textBoxPeaje_TextChanged);
            // 
            // buttonInsertarVehiculo
            // 
            this.buttonInsertarVehiculo.Location = new System.Drawing.Point(87, 139);
            this.buttonInsertarVehiculo.Name = "buttonInsertarVehiculo";
            this.buttonInsertarVehiculo.Size = new System.Drawing.Size(109, 24);
            this.buttonInsertarVehiculo.TabIndex = 8;
            this.buttonInsertarVehiculo.Text = "añadir";
            this.buttonInsertarVehiculo.UseVisualStyleBackColor = true;
            this.buttonInsertarVehiculo.Click += new System.EventHandler(this.buttonInsertarVehiculo_Click);
            // 
            // labelImprimirVehiculo
            // 
            this.labelImprimirVehiculo.AutoSize = true;
            this.labelImprimirVehiculo.Location = new System.Drawing.Point(22, 189);
            this.labelImprimirVehiculo.Name = "labelImprimirVehiculo";
            this.labelImprimirVehiculo.Size = new System.Drawing.Size(205, 13);
            this.labelImprimirVehiculo.TabIndex = 9;
            this.labelImprimirVehiculo.Text = "Informacion del ultimo vehiuculo añadido: ";
            // 
            // labelTotalRecaudado
            // 
            this.labelTotalRecaudado.AutoSize = true;
            this.labelTotalRecaudado.Location = new System.Drawing.Point(22, 212);
            this.labelTotalRecaudado.Name = "labelTotalRecaudado";
            this.labelTotalRecaudado.Size = new System.Drawing.Size(91, 13);
            this.labelTotalRecaudado.TabIndex = 10;
            this.labelTotalRecaudado.Text = "Total recaudado: ";
            this.labelTotalRecaudado.Click += new System.EventHandler(this.labelTotalRecaudado_Click);
            // 
            // labelTotalOcupantesRegistrados
            // 
            this.labelTotalOcupantesRegistrados.AutoSize = true;
            this.labelTotalOcupantesRegistrados.Location = new System.Drawing.Point(22, 234);
            this.labelTotalOcupantesRegistrados.Name = "labelTotalOcupantesRegistrados";
            this.labelTotalOcupantesRegistrados.Size = new System.Drawing.Size(89, 13);
            this.labelTotalOcupantesRegistrados.TabIndex = 11;
            this.labelTotalOcupantesRegistrados.Text = "Total Ocupantes:";
            this.labelTotalOcupantesRegistrados.Click += new System.EventHandler(this.labelTotalOcupantesRegistrados_Click);
            // 
            // labelVehiculosRegistrados
            // 
            this.labelVehiculosRegistrados.AutoSize = true;
            this.labelVehiculosRegistrados.Location = new System.Drawing.Point(22, 257);
            this.labelVehiculosRegistrados.Name = "labelVehiculosRegistrados";
            this.labelVehiculosRegistrados.Size = new System.Drawing.Size(83, 13);
            this.labelVehiculosRegistrados.TabIndex = 12;
            this.labelVehiculosRegistrados.Text = "Total Vehiculos:";
            this.labelVehiculosRegistrados.Click += new System.EventHandler(this.labelVehiculosRegistrados_Click);
            // 
            // labelVehiculoMasOcupado
            // 
            this.labelVehiculoMasOcupado.AutoSize = true;
            this.labelVehiculoMasOcupado.Location = new System.Drawing.Point(22, 279);
            this.labelVehiculoMasOcupado.Name = "labelVehiculoMasOcupado";
            this.labelVehiculoMasOcupado.Size = new System.Drawing.Size(118, 13);
            this.labelVehiculoMasOcupado.TabIndex = 13;
            this.labelVehiculoMasOcupado.Text = "Vehiculo mas ocupado:";
            this.labelVehiculoMasOcupado.Click += new System.EventHandler(this.labelVehiculoMasOcupado_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 369);
            this.Controls.Add(this.labelVehiculoMasOcupado);
            this.Controls.Add(this.labelVehiculosRegistrados);
            this.Controls.Add(this.labelTotalOcupantesRegistrados);
            this.Controls.Add(this.labelTotalRecaudado);
            this.Controls.Add(this.labelImprimirVehiculo);
            this.Controls.Add(this.buttonInsertarVehiculo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxPeaje);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxOcupantes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxMatricula);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxTipo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxTipo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMatricula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxOcupantes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPeaje;
        private System.Windows.Forms.Button buttonInsertarVehiculo;
        private System.Windows.Forms.Label labelImprimirVehiculo;
        private System.Windows.Forms.Label labelTotalRecaudado;
        private System.Windows.Forms.Label labelTotalOcupantesRegistrados;
        private System.Windows.Forms.Label labelVehiculosRegistrados;
        private System.Windows.Forms.Label labelVehiculoMasOcupado;
    }
}

