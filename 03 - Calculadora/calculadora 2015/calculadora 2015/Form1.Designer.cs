﻿namespace _03___Calculadora
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonNum7 = new System.Windows.Forms.Button();
            this.buttonNum8 = new System.Windows.Forms.Button();
            this.buttonNum9 = new System.Windows.Forms.Button();
            this.buttonNum4 = new System.Windows.Forms.Button();
            this.buttonNum5 = new System.Windows.Forms.Button();
            this.buttonNum6 = new System.Windows.Forms.Button();
            this.buttonNum1 = new System.Windows.Forms.Button();
            this.buttonNum2 = new System.Windows.Forms.Button();
            this.buttonNum3 = new System.Windows.Forms.Button();
            this.buttonNum0 = new System.Windows.Forms.Button();
            this.buttonCharComa = new System.Windows.Forms.Button();
            this.buttonLimpiar = new System.Windows.Forms.Button();
            this.buttonOperSuma = new System.Windows.Forms.Button();
            this.buttonOperResta = new System.Windows.Forms.Button();
            this.buttonOperMulti = new System.Windows.Forms.Button();
            this.buttonOperDivi = new System.Windows.Forms.Button();
            this.buttonOperFact = new System.Windows.Forms.Button();
            this.buttonOperElevadoXY = new System.Windows.Forms.Button();
            this.buttonOperElevadoX2 = new System.Windows.Forms.Button();
            this.buttonOperElevado10X = new System.Windows.Forms.Button();
            this.buttonOper1entreX = new System.Windows.Forms.Button();
            this.buttonCalcLog = new System.Windows.Forms.Button();
            this.buttonCalcIgual = new System.Windows.Forms.Button();
            this.buttonCalcRaiz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(10, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(269, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttonNum7
            // 
            this.buttonNum7.Location = new System.Drawing.Point(10, 71);
            this.buttonNum7.Name = "buttonNum7";
            this.buttonNum7.Size = new System.Drawing.Size(38, 39);
            this.buttonNum7.TabIndex = 1;
            this.buttonNum7.Text = "7";
            this.buttonNum7.UseVisualStyleBackColor = true;
            this.buttonNum7.Click += new System.EventHandler(this.buttonNum7_Click);
            // 
            // buttonNum8
            // 
            this.buttonNum8.Location = new System.Drawing.Point(53, 71);
            this.buttonNum8.Name = "buttonNum8";
            this.buttonNum8.Size = new System.Drawing.Size(38, 39);
            this.buttonNum8.TabIndex = 2;
            this.buttonNum8.Text = "8";
            this.buttonNum8.UseVisualStyleBackColor = true;
            this.buttonNum8.Click += new System.EventHandler(this.buttonNum8_Click);
            // 
            // buttonNum9
            // 
            this.buttonNum9.Location = new System.Drawing.Point(96, 71);
            this.buttonNum9.Name = "buttonNum9";
            this.buttonNum9.Size = new System.Drawing.Size(40, 39);
            this.buttonNum9.TabIndex = 3;
            this.buttonNum9.Text = "9";
            this.buttonNum9.UseVisualStyleBackColor = true;
            this.buttonNum9.Click += new System.EventHandler(this.buttonNum9_Click);
            // 
            // buttonNum4
            // 
            this.buttonNum4.Location = new System.Drawing.Point(10, 115);
            this.buttonNum4.Name = "buttonNum4";
            this.buttonNum4.Size = new System.Drawing.Size(37, 36);
            this.buttonNum4.TabIndex = 4;
            this.buttonNum4.Text = "4";
            this.buttonNum4.UseVisualStyleBackColor = true;
            this.buttonNum4.Click += new System.EventHandler(this.buttonNum4_Click);
            // 
            // buttonNum5
            // 
            this.buttonNum5.Location = new System.Drawing.Point(52, 115);
            this.buttonNum5.Name = "buttonNum5";
            this.buttonNum5.Size = new System.Drawing.Size(38, 36);
            this.buttonNum5.TabIndex = 5;
            this.buttonNum5.Text = "5";
            this.buttonNum5.UseVisualStyleBackColor = true;
            this.buttonNum5.Click += new System.EventHandler(this.buttonNum5_Click);
            // 
            // buttonNum6
            // 
            this.buttonNum6.Location = new System.Drawing.Point(96, 115);
            this.buttonNum6.Name = "buttonNum6";
            this.buttonNum6.Size = new System.Drawing.Size(39, 36);
            this.buttonNum6.TabIndex = 6;
            this.buttonNum6.Text = "6";
            this.buttonNum6.UseVisualStyleBackColor = true;
            this.buttonNum6.Click += new System.EventHandler(this.buttonNum6_Click);
            // 
            // buttonNum1
            // 
            this.buttonNum1.Location = new System.Drawing.Point(10, 157);
            this.buttonNum1.Name = "buttonNum1";
            this.buttonNum1.Size = new System.Drawing.Size(38, 36);
            this.buttonNum1.TabIndex = 7;
            this.buttonNum1.Text = "1";
            this.buttonNum1.UseVisualStyleBackColor = true;
            this.buttonNum1.Click += new System.EventHandler(this.buttonNum1_Click);
            // 
            // buttonNum2
            // 
            this.buttonNum2.Location = new System.Drawing.Point(52, 157);
            this.buttonNum2.Name = "buttonNum2";
            this.buttonNum2.Size = new System.Drawing.Size(39, 36);
            this.buttonNum2.TabIndex = 8;
            this.buttonNum2.Text = "2";
            this.buttonNum2.UseVisualStyleBackColor = true;
            this.buttonNum2.Click += new System.EventHandler(this.buttonNum2_Click);
            // 
            // buttonNum3
            // 
            this.buttonNum3.Location = new System.Drawing.Point(96, 157);
            this.buttonNum3.Name = "buttonNum3";
            this.buttonNum3.Size = new System.Drawing.Size(39, 36);
            this.buttonNum3.TabIndex = 9;
            this.buttonNum3.Text = "3";
            this.buttonNum3.UseVisualStyleBackColor = true;
            this.buttonNum3.Click += new System.EventHandler(this.buttonNum3_Click);
            // 
            // buttonNum0
            // 
            this.buttonNum0.Location = new System.Drawing.Point(52, 197);
            this.buttonNum0.Name = "buttonNum0";
            this.buttonNum0.Size = new System.Drawing.Size(39, 36);
            this.buttonNum0.TabIndex = 10;
            this.buttonNum0.Text = "0";
            this.buttonNum0.UseVisualStyleBackColor = true;
            this.buttonNum0.Click += new System.EventHandler(this.buttonNum0_Click);
            // 
            // buttonCharComa
            // 
            this.buttonCharComa.Location = new System.Drawing.Point(96, 197);
            this.buttonCharComa.Name = "buttonCharComa";
            this.buttonCharComa.Size = new System.Drawing.Size(39, 36);
            this.buttonCharComa.TabIndex = 11;
            this.buttonCharComa.Text = ",";
            this.buttonCharComa.UseVisualStyleBackColor = true;
            this.buttonCharComa.Click += new System.EventHandler(this.buttonCharComa_Click);
            // 
            // buttonLimpiar
            // 
            this.buttonLimpiar.Location = new System.Drawing.Point(10, 250);
            this.buttonLimpiar.Name = "buttonLimpiar";
            this.buttonLimpiar.Size = new System.Drawing.Size(64, 20);
            this.buttonLimpiar.TabIndex = 12;
            this.buttonLimpiar.Text = "Limpiar";
            this.buttonLimpiar.UseVisualStyleBackColor = true;
            this.buttonLimpiar.Click += new System.EventHandler(this.buttonLimpiar_Click);
            // 
            // buttonOperSuma
            // 
            this.buttonOperSuma.Location = new System.Drawing.Point(189, 72);
            this.buttonOperSuma.Name = "buttonOperSuma";
            this.buttonOperSuma.Size = new System.Drawing.Size(41, 36);
            this.buttonOperSuma.TabIndex = 13;
            this.buttonOperSuma.Text = "+";
            this.buttonOperSuma.UseVisualStyleBackColor = true;
            this.buttonOperSuma.Click += new System.EventHandler(this.buttonOperSuma_Click);
            // 
            // buttonOperResta
            // 
            this.buttonOperResta.Location = new System.Drawing.Point(189, 116);
            this.buttonOperResta.Name = "buttonOperResta";
            this.buttonOperResta.Size = new System.Drawing.Size(41, 36);
            this.buttonOperResta.TabIndex = 14;
            this.buttonOperResta.Text = "-";
            this.buttonOperResta.UseVisualStyleBackColor = true;
            this.buttonOperResta.Click += new System.EventHandler(this.buttonOperResta_Click);
            // 
            // buttonOperMulti
            // 
            this.buttonOperMulti.Location = new System.Drawing.Point(189, 157);
            this.buttonOperMulti.Name = "buttonOperMulti";
            this.buttonOperMulti.Size = new System.Drawing.Size(41, 36);
            this.buttonOperMulti.TabIndex = 15;
            this.buttonOperMulti.Text = "x";
            this.buttonOperMulti.UseVisualStyleBackColor = true;
            this.buttonOperMulti.Click += new System.EventHandler(this.buttonOperMulti_Click);
            // 
            // buttonOperDivi
            // 
            this.buttonOperDivi.Location = new System.Drawing.Point(189, 197);
            this.buttonOperDivi.Name = "buttonOperDivi";
            this.buttonOperDivi.Size = new System.Drawing.Size(41, 35);
            this.buttonOperDivi.TabIndex = 16;
            this.buttonOperDivi.Text = "/";
            this.buttonOperDivi.UseVisualStyleBackColor = true;
            this.buttonOperDivi.Click += new System.EventHandler(this.buttonOperDivi_Click);
            // 
            // buttonOperFact
            // 
            this.buttonOperFact.Location = new System.Drawing.Point(236, 71);
            this.buttonOperFact.Name = "buttonOperFact";
            this.buttonOperFact.Size = new System.Drawing.Size(43, 36);
            this.buttonOperFact.TabIndex = 17;
            this.buttonOperFact.Text = "!";
            this.buttonOperFact.UseVisualStyleBackColor = true;
            this.buttonOperFact.Click += new System.EventHandler(this.buttonOperFact_Click);
            // 
            // buttonOperElevadoXY
            // 
            this.buttonOperElevadoXY.Location = new System.Drawing.Point(237, 116);
            this.buttonOperElevadoXY.Name = "buttonOperElevadoXY";
            this.buttonOperElevadoXY.Size = new System.Drawing.Size(42, 36);
            this.buttonOperElevadoXY.TabIndex = 18;
            this.buttonOperElevadoXY.Text = "X^Y";
            this.buttonOperElevadoXY.UseVisualStyleBackColor = true;
            this.buttonOperElevadoXY.Click += new System.EventHandler(this.buttonOperElevadoXY_Click);
            // 
            // buttonOperElevadoX2
            // 
            this.buttonOperElevadoX2.Location = new System.Drawing.Point(237, 158);
            this.buttonOperElevadoX2.Name = "buttonOperElevadoX2";
            this.buttonOperElevadoX2.Size = new System.Drawing.Size(42, 35);
            this.buttonOperElevadoX2.TabIndex = 19;
            this.buttonOperElevadoX2.Text = "X^2";
            this.buttonOperElevadoX2.UseVisualStyleBackColor = true;
            this.buttonOperElevadoX2.Click += new System.EventHandler(this.buttonOperElevadoX2_Click);
            // 
            // buttonOperElevado10X
            // 
            this.buttonOperElevado10X.Location = new System.Drawing.Point(237, 197);
            this.buttonOperElevado10X.Name = "buttonOperElevado10X";
            this.buttonOperElevado10X.Size = new System.Drawing.Size(42, 35);
            this.buttonOperElevado10X.TabIndex = 20;
            this.buttonOperElevado10X.Text = "10^X";
            this.buttonOperElevado10X.UseVisualStyleBackColor = true;
            this.buttonOperElevado10X.Click += new System.EventHandler(this.buttonOperElevado10X_Click);
            // 
            // buttonOper1entreX
            // 
            this.buttonOper1entreX.Location = new System.Drawing.Point(189, 237);
            this.buttonOper1entreX.Name = "buttonOper1entreX";
            this.buttonOper1entreX.Size = new System.Drawing.Size(41, 33);
            this.buttonOper1entreX.TabIndex = 21;
            this.buttonOper1entreX.Text = "1/X";
            this.buttonOper1entreX.UseVisualStyleBackColor = true;
            this.buttonOper1entreX.Click += new System.EventHandler(this.buttonOper1entreX_Click);
            // 
            // buttonCalcLog
            // 
            this.buttonCalcLog.Location = new System.Drawing.Point(236, 237);
            this.buttonCalcLog.Name = "buttonCalcLog";
            this.buttonCalcLog.Size = new System.Drawing.Size(43, 33);
            this.buttonCalcLog.TabIndex = 22;
            this.buttonCalcLog.Text = "LogxY";
            this.buttonCalcLog.UseVisualStyleBackColor = true;
            this.buttonCalcLog.Click += new System.EventHandler(this.buttonCalcLog_Click);
            // 
            // buttonCalcIgual
            // 
            this.buttonCalcIgual.Location = new System.Drawing.Point(10, 197);
            this.buttonCalcIgual.Name = "buttonCalcIgual";
            this.buttonCalcIgual.Size = new System.Drawing.Size(38, 36);
            this.buttonCalcIgual.TabIndex = 23;
            this.buttonCalcIgual.Text = "=";
            this.buttonCalcIgual.UseVisualStyleBackColor = true;
            this.buttonCalcIgual.Click += new System.EventHandler(this.buttonCalcIgual_Click);
            // 
            // buttonCalcRaiz
            // 
            this.buttonCalcRaiz.Location = new System.Drawing.Point(96, 238);
            this.buttonCalcRaiz.Name = "buttonCalcRaiz";
            this.buttonCalcRaiz.Size = new System.Drawing.Size(39, 32);
            this.buttonCalcRaiz.TabIndex = 24;
            this.buttonCalcRaiz.Text = "√";
            this.buttonCalcRaiz.UseVisualStyleBackColor = true;
            this.buttonCalcRaiz.Click += new System.EventHandler(this.buttonCalcRaiz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 308);
            this.Controls.Add(this.buttonCalcRaiz);
            this.Controls.Add(this.buttonCalcIgual);
            this.Controls.Add(this.buttonCalcLog);
            this.Controls.Add(this.buttonOper1entreX);
            this.Controls.Add(this.buttonOperElevado10X);
            this.Controls.Add(this.buttonOperElevadoX2);
            this.Controls.Add(this.buttonOperElevadoXY);
            this.Controls.Add(this.buttonOperFact);
            this.Controls.Add(this.buttonOperDivi);
            this.Controls.Add(this.buttonOperMulti);
            this.Controls.Add(this.buttonOperResta);
            this.Controls.Add(this.buttonOperSuma);
            this.Controls.Add(this.buttonLimpiar);
            this.Controls.Add(this.buttonCharComa);
            this.Controls.Add(this.buttonNum0);
            this.Controls.Add(this.buttonNum3);
            this.Controls.Add(this.buttonNum2);
            this.Controls.Add(this.buttonNum1);
            this.Controls.Add(this.buttonNum6);
            this.Controls.Add(this.buttonNum5);
            this.Controls.Add(this.buttonNum4);
            this.Controls.Add(this.buttonNum9);
            this.Controls.Add(this.buttonNum8);
            this.Controls.Add(this.buttonNum7);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonNum7;
        private System.Windows.Forms.Button buttonNum8;
        private System.Windows.Forms.Button buttonNum9;
        private System.Windows.Forms.Button buttonNum4;
        private System.Windows.Forms.Button buttonNum5;
        private System.Windows.Forms.Button buttonNum6;
        private System.Windows.Forms.Button buttonNum1;
        private System.Windows.Forms.Button buttonNum2;
        private System.Windows.Forms.Button buttonNum3;
        private System.Windows.Forms.Button buttonNum0;
        private System.Windows.Forms.Button buttonCharComa;
        private System.Windows.Forms.Button buttonLimpiar;
        private System.Windows.Forms.Button buttonOperSuma;
        private System.Windows.Forms.Button buttonOperResta;
        private System.Windows.Forms.Button buttonOperMulti;
        private System.Windows.Forms.Button buttonOperDivi;
        private System.Windows.Forms.Button buttonOperFact;
        private System.Windows.Forms.Button buttonOperElevadoXY;
        private System.Windows.Forms.Button buttonOperElevadoX2;
        private System.Windows.Forms.Button buttonOperElevado10X;
        private System.Windows.Forms.Button buttonOper1entreX;
        private System.Windows.Forms.Button buttonCalcLog;
        private System.Windows.Forms.Button buttonCalcIgual;
        private System.Windows.Forms.Button buttonCalcRaiz;
    }
}

