﻿using calculadora_2015;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03___Calculadora
{
    public partial class Form1 : Form
    {
        // Declaración de variables

        private double num1;
        private double num2;
        private double resul;

        private int eleccion;

        public Form1()
        {
            num1 = 0;
            num2 = 0;
            resul = 0;
            eleccion = 0;
            InitializeComponent();
        }


        // Botones para los números
        private void buttonNum5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "5";
            else
                textBox1.Text = textBox1.Text + "5";

        }

        private void buttonNum0_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "0";
        }

        private void buttonNum7_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "7";
            else
                textBox1.Text = textBox1.Text + "7";

        }

        private void buttonNum8_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "8";
            else
                textBox1.Text = textBox1.Text + "8";

        }

        private void buttonNum9_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "9";
            else
                textBox1.Text = textBox1.Text + "9";

        }

        private void buttonNum4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "4";
            else
                textBox1.Text = textBox1.Text + "4";

        }

        private void buttonNum6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "6";
            else
                textBox1.Text = textBox1.Text + "6";

        }

        private void buttonNum1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "1";
            else
                textBox1.Text = textBox1.Text + "1";

        }

        private void buttonNum2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "2";
            else
                textBox1.Text = textBox1.Text + "2";

        }

        private void buttonNum3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "3";
            else
                textBox1.Text = textBox1.Text + "3";

        }



        // botones de control
        private void buttonCharComa_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == " ")
                textBox1.Text = "0" + ",";
            else
                textBox1.Text = textBox1.Text + ",";
        }

        private void buttonLimpiar_Click(object sender, EventArgs e)
        {
            textBox1.Text = " ";
        }



        //botones de operaciones
        private void buttonOperSuma_Click(object sender, EventArgs e)
        {
            if (num1 == 0)
            {
                eleccion = 1;
                num1 = Convert.ToDouble(textBox1.Text);
                textBox1.Text = " ";
            }


        }

        private void buttonOperFact_Click(object sender, EventArgs e)
        {
            double resultado = 0;
            Metodos.factorial(num1, ref resultado);
            textBox1.Text = Convert.ToString(resultado);
        }
        

        private void buttonOperResta_Click(object sender, EventArgs e)
        {
            if (num1 == 0)
            {
                eleccion = 2;
                num1 = Convert.ToDouble(textBox1.Text);
                textBox1.Text = " ";
            }

        }

        private void buttonOperElevadoXY_Click(object sender, EventArgs e)
        {
            if (num1 == 0)
            {
                eleccion = 5;
                num1 = Convert.ToDouble(textBox1.Text);
                textBox1.Text = " ";
            }

        }

        private void buttonOperMulti_Click(object sender, EventArgs e)
        {
            if (num1 == 0)
            {
                eleccion = 3;
                num1 = Convert.ToDouble(textBox1.Text);
                textBox1.Text = " ";
            }

        }

        private void buttonOperElevadoX2_Click(object sender, EventArgs e)
        {
            double resultado = 0;
            Metodos.potencia2(num1, ref resultado);
            textBox1.Text = Convert.ToString(resultado);

        }

        private void buttonOperDivi_Click(object sender, EventArgs e)
        {
            if (num1 == 0)
            {
                eleccion = 4;
                num1 = Convert.ToDouble(textBox1.Text);
                textBox1.Text = " ";
            }

        }

        private void buttonCalcRaiz_Click(object sender, EventArgs e)
        {
            num1 = Convert.ToDouble(textBox1.Text);

            resul = Math.Sqrt(num1);
            textBox1.Text = Convert.ToString(resul);
            resul = 0;
            num1 = 0;

        }

        private void buttonOperElevado10X_Click(object sender, EventArgs e)
        {
            num1 = Convert.ToDouble(textBox1.Text);

            resul = Math.Pow(10, num1);
            textBox1.Text = Convert.ToString(resul);
            num1 = 0;


        }

        private void buttonOper1entreX_Click(object sender, EventArgs e)
        {
            num1 = Convert.ToDouble(textBox1.Text);

            if (num1 == 0)
                textBox1.Text = "1";
            else
                calc1Entrex();

        }

        private void buttonCalcLog_Click(object sender, EventArgs e)
        {
            if (num1 == 0)
            {
                eleccion = 6;
                num1 = Convert.ToDouble(textBox1.Text);
                textBox1.Text = " ";
            }

        }


        //Botones para mostrar datos
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonCalcIgual_Click(object sender, EventArgs e)
        {
            num2 = Convert.ToDouble(textBox1.Text);

            switch (eleccion)
            {
                case 1:
                    resul = Metodos.suma(num1, num2);
                    textBox1.Text = Convert.ToString(resul);
                    eleccion = 0;
                    num1 = 0;
                    num2 = 0;
                    break;

                case 2:
                    resul = Metodos.resta(num1, num2);
                    textBox1.Text = Convert.ToString(resul);
                    eleccion = 0;
                    num1 = 0;
                    num2 = 0;
                    break;

                case 3:
                    resul = Metodos.multiplicacion(num1, num2);
                    textBox1.Text = Convert.ToString(resul);
                    eleccion = 0;
                    num1 = 0;
                    num2 = 0;
                    break;

                case 4:
                    resul = Metodos.division(num1, num2);
                    textBox1.Text = Convert.ToString(resul);
                    eleccion = 0;
                    num1 = 0;
                    num2 = 0;
                    break;

                case 5:
                    Metodos.potencia(num1, num2, ref resul);
                    eleccion = 0;
                    num1 = 0;
                    num2 = 0;
                    break;

                case 6:
                    Metodos.logaritmo(num1, num2, ref resul);
                    eleccion = 0;
                    num1 = 0;
                    num2 = 0;
                    break;

            }

        }

        public void calc1Entrex()
        {
            textBox1.Text = Convert.ToString(1 / num1);
        }

        public void calc10ElevadoX()
        {
            resul = Math.Pow(10, num1);
            textBox1.Text = Convert.ToString(resul);
        }


    }
}

