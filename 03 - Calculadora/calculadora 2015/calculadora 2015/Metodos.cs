﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculadora_2015
{
    class Metodos
    {
        public static double suma(double n1, double n2)
        {
            return n1 + n2;
        }

        public static double resta(double n1, double n2)
        {
            return n1 - n2;
        }

        public static double multiplicacion(double n1, double n2)
        {
            return n1 * n2;
        }

        public static double division(double n1, double n2)
        {
            return n1 / n2;
        }

        public static void factorial (double n1, ref double resul)
            
        {
            for (double i = n1; i >= 1; i--)
                resul *= i;
            
        }
        
        public static void potencia (double n1, double n2, ref double res)
        {
            res =  Math.Pow(n1, n2);
        }

        public static void potencia2 (double n1, ref double res)              // Potencia al cuadrado
        {
            res = Math.Pow(n1, 2);
        }

        public static void logaritmo(double n1, double n2, ref double res)
        {
            res = Math.Log(n1, n2);
        }
    }
}
