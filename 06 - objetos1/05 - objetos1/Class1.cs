﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05___objetos1
{
    class Circulo : Figura
    {
        private double radio;

        public Circulo(string color, int px, int py, double r) : base(color, px, py)
        {
            radio = r;
        }

        public Circulo()
        {
            radio = 0.0;
        }

        public double Area()
        {
            return 3.1416 * Math.Pow(radio, 2);
        }

        public override string Escribir()
        {
            return "Circulo: (Radio) " + radio + "" + base.Escribir();
        }
    }
}
