﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05___objetos1
{
    class Cuadrado : Figura
    {
        private double lado;

        public Cuadrado (double la, String color, int pX, int pY) : base(color, pX, pY)
        {
            lado = la;
        }

        public Cuadrado() : base ()
        {
            lado = 0.0;
        }

        public double Area()
        {
            return lado * lado;
        }

        public override string Escribir()
        {
            return "Cuadrado: (lado) " + lado + "" + base.Escribir();
        }

    }
}
