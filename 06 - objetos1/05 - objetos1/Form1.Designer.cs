﻿namespace _05___objetos1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonArea = new System.Windows.Forms.Button();
            this.labelRadio = new System.Windows.Forms.Label();
            this.textBoxRadio = new System.Windows.Forms.TextBox();
            this.labelResul = new System.Windows.Forms.Label();
            this.buttonCrearCirculo = new System.Windows.Forms.Button();
            this.buttonCrearCuadrado = new System.Windows.Forms.Button();
            this.buttonCrearTriangulo = new System.Windows.Forms.Button();
            this.textBoxLado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxBase = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAltura = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPosX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPosY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonMostrarTabla = new System.Windows.Forms.Button();
            this.labelResultadoTabla = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonArea
            // 
            this.buttonArea.Location = new System.Drawing.Point(71, 222);
            this.buttonArea.Name = "buttonArea";
            this.buttonArea.Size = new System.Drawing.Size(75, 23);
            this.buttonArea.TabIndex = 0;
            this.buttonArea.Text = "Calcular";
            this.buttonArea.UseVisualStyleBackColor = true;
            this.buttonArea.Click += new System.EventHandler(this.buttonArea_Click);
            // 
            // labelRadio
            // 
            this.labelRadio.AutoSize = true;
            this.labelRadio.Location = new System.Drawing.Point(12, 31);
            this.labelRadio.Name = "labelRadio";
            this.labelRadio.Size = new System.Drawing.Size(35, 13);
            this.labelRadio.TabIndex = 1;
            this.labelRadio.Text = "Radio";
            // 
            // textBoxRadio
            // 
            this.textBoxRadio.Location = new System.Drawing.Point(71, 31);
            this.textBoxRadio.Name = "textBoxRadio";
            this.textBoxRadio.Size = new System.Drawing.Size(100, 20);
            this.textBoxRadio.TabIndex = 2;
            // 
            // labelResul
            // 
            this.labelResul.AutoSize = true;
            this.labelResul.Location = new System.Drawing.Point(68, 248);
            this.labelResul.Name = "labelResul";
            this.labelResul.Size = new System.Drawing.Size(61, 13);
            this.labelResul.TabIndex = 3;
            this.labelResul.Text = "Resultado: ";
            // 
            // buttonCrearCirculo
            // 
            this.buttonCrearCirculo.Location = new System.Drawing.Point(258, 26);
            this.buttonCrearCirculo.Name = "buttonCrearCirculo";
            this.buttonCrearCirculo.Size = new System.Drawing.Size(101, 23);
            this.buttonCrearCirculo.TabIndex = 4;
            this.buttonCrearCirculo.Text = "Crear Circulo";
            this.buttonCrearCirculo.UseVisualStyleBackColor = true;
            this.buttonCrearCirculo.Click += new System.EventHandler(this.buttonCrearCirculo_Click);
            // 
            // buttonCrearCuadrado
            // 
            this.buttonCrearCuadrado.Location = new System.Drawing.Point(258, 55);
            this.buttonCrearCuadrado.Name = "buttonCrearCuadrado";
            this.buttonCrearCuadrado.Size = new System.Drawing.Size(101, 25);
            this.buttonCrearCuadrado.TabIndex = 5;
            this.buttonCrearCuadrado.Text = "Crear Cuadrado";
            this.buttonCrearCuadrado.UseVisualStyleBackColor = true;
            this.buttonCrearCuadrado.Click += new System.EventHandler(this.buttonCrearCuadrado_Click);
            // 
            // buttonCrearTriangulo
            // 
            this.buttonCrearTriangulo.Location = new System.Drawing.Point(258, 114);
            this.buttonCrearTriangulo.Name = "buttonCrearTriangulo";
            this.buttonCrearTriangulo.Size = new System.Drawing.Size(101, 25);
            this.buttonCrearTriangulo.TabIndex = 6;
            this.buttonCrearTriangulo.Text = "Crear Triangulo";
            this.buttonCrearTriangulo.UseVisualStyleBackColor = true;
            this.buttonCrearTriangulo.Click += new System.EventHandler(this.buttonCrearTriangulo_Click);
            // 
            // textBoxLado
            // 
            this.textBoxLado.Location = new System.Drawing.Point(71, 60);
            this.textBoxLado.Name = "textBoxLado";
            this.textBoxLado.Size = new System.Drawing.Size(100, 20);
            this.textBoxLado.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Lado";
            // 
            // textBoxBase
            // 
            this.textBoxBase.Location = new System.Drawing.Point(71, 91);
            this.textBoxBase.Name = "textBoxBase";
            this.textBoxBase.Size = new System.Drawing.Size(100, 20);
            this.textBoxBase.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Base";
            // 
            // textBoxAltura
            // 
            this.textBoxAltura.Location = new System.Drawing.Point(71, 117);
            this.textBoxAltura.Name = "textBoxAltura";
            this.textBoxAltura.Size = new System.Drawing.Size(100, 20);
            this.textBoxAltura.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Altura";
            // 
            // textBoxPosX
            // 
            this.textBoxPosX.Location = new System.Drawing.Point(216, 181);
            this.textBoxPosX.Name = "textBoxPosX";
            this.textBoxPosX.Size = new System.Drawing.Size(100, 20);
            this.textBoxPosX.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(176, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "PosX";
            // 
            // textBoxPosY
            // 
            this.textBoxPosY.Location = new System.Drawing.Point(382, 181);
            this.textBoxPosY.Name = "textBoxPosY";
            this.textBoxPosY.Size = new System.Drawing.Size(100, 20);
            this.textBoxPosY.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(342, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "PosY";
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(52, 181);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(100, 20);
            this.textBoxColor.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Color";
            // 
            // buttonMostrarTabla
            // 
            this.buttonMostrarTabla.Location = new System.Drawing.Point(216, 222);
            this.buttonMostrarTabla.Name = "buttonMostrarTabla";
            this.buttonMostrarTabla.Size = new System.Drawing.Size(99, 23);
            this.buttonMostrarTabla.TabIndex = 19;
            this.buttonMostrarTabla.Text = "Mostrar tabla";
            this.buttonMostrarTabla.UseVisualStyleBackColor = true;
            this.buttonMostrarTabla.Click += new System.EventHandler(this.buttonMostrarTabla_Click);
            // 
            // labelResultadoTabla
            // 
            this.labelResultadoTabla.AutoSize = true;
            this.labelResultadoTabla.Location = new System.Drawing.Point(216, 252);
            this.labelResultadoTabla.Name = "labelResultadoTabla";
            this.labelResultadoTabla.Size = new System.Drawing.Size(0, 13);
            this.labelResultadoTabla.TabIndex = 20;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 382);
            this.Controls.Add(this.labelResultadoTabla);
            this.Controls.Add(this.buttonMostrarTabla);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxPosY);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxPosX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxAltura);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxBase);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxLado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCrearTriangulo);
            this.Controls.Add(this.buttonCrearCuadrado);
            this.Controls.Add(this.buttonCrearCirculo);
            this.Controls.Add(this.labelResul);
            this.Controls.Add(this.textBoxRadio);
            this.Controls.Add(this.labelRadio);
            this.Controls.Add(this.buttonArea);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonArea;
        private System.Windows.Forms.Label labelRadio;
        private System.Windows.Forms.TextBox textBoxRadio;
        private System.Windows.Forms.Label labelResul;
        private System.Windows.Forms.Button buttonCrearCirculo;
        private System.Windows.Forms.Button buttonCrearCuadrado;
        private System.Windows.Forms.Button buttonCrearTriangulo;
        private System.Windows.Forms.TextBox textBoxLado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxBase;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAltura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPosX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPosY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonMostrarTabla;
        private System.Windows.Forms.Label labelResultadoTabla;
    }
}

