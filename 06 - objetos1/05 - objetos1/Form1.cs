﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05___objetos1
{
    public partial class Form1 : Form
    {
        private Figura[] tablaFiguras;
        private int posicionActual;

        public Form1()
        {
            InitializeComponent();
            tablaFiguras = new Figura[8];
            posicionActual = 0;
        }

        private void buttonArea_Click(object sender, EventArgs e)
        {

            Circulo miCirculo = new Circulo(textBoxColor.Text, Int32.Parse(textBoxPosX.Text), Int32.Parse(textBoxPosY.Text), double.Parse(textBoxRadio.Text));
            tablaFiguras[posicionActual] = miCirculo;

            posicionActual++;

        }

        private void buttonCrearCuadrado_Click(object sender, EventArgs e)
        {
            Cuadrado miCuadrado = new Cuadrado(double.Parse(textBoxLado.Text), textBoxColor.Text, int.Parse(textBoxPosX.Text), int.Parse(textBoxPosY.Text));
            tablaFiguras[posicionActual] = miCuadrado;

            posicionActual++;
        }

        private void buttonCrearTriangulo_Click(object sender, EventArgs e)
        {
            Triangulo miTriangulo = new Triangulo(double.Parse(textBoxAltura.Text), double.Parse(textBoxBase.Text), textBoxColor.Text, int.Parse(textBoxPosX.Text), int.Parse(textBoxPosY.Text));
            tablaFiguras[posicionActual] = miTriangulo;

            posicionActual++;
        }


        private void buttonCrearCirculo_Click(object sender, EventArgs e)
        {
            Circulo miCirculo = new Circulo(textBoxColor.Text, Int32.Parse(textBoxPosX.Text), Int32.Parse(textBoxPosY.Text), double.Parse(textBoxRadio.Text));
            tablaFiguras[posicionActual] = miCirculo;

            posicionActual++;

        }

        private void buttonMostrarTabla_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < posicionActual; i++)
            {
                labelResultadoTabla.Text += tablaFiguras[i].Escribir() + "\n";
            }
        }


    }
}
