﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05___objetos1
{
    class Triangulo : Figura
    {
        private double altura;
        private double bAse;

        public Triangulo (double al, double ba, String color, int pX, int pY) : base(color, pX, pY)
        {
            altura = al;
            bAse = ba;
        }

        public Triangulo () : base()
        {
            altura = 0.0;
            bAse = 0.0;
        }


        public double Area()
        {
            return (bAse * altura) / 2;
        }


        public override string Escribir()
        {
            return "Triangulo: (Base) " + bAse + "(Altura)" + altura + "" + base.Escribir();
        }

    }
}
