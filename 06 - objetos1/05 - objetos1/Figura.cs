﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05___objetos1
{
    class Figura
    {
        private string color;
        private int posicionX;
        private int posicionY;

        public Figura()
        {
            color = "";
            posicionX = -1;
            posicionY = -1;
        }

        public Figura (String c, int x, int y)
        {
            color = c;
            posicionX = x;
            posicionY = y;
        }


        virtual public String Escribir()
        {
            return "Es una figura. Color " + color + " (x,y) " + posicionX + "," + posicionY;
        }

    }
}
