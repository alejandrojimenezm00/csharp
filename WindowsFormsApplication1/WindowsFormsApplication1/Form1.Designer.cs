﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEdad = new System.Windows.Forms.Label();
            this.textBoxEdad = new System.Windows.Forms.TextBox();
            this.labelCalculo = new System.Windows.Forms.Label();
            this.buttonCalcularEdad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelEdad
            // 
            this.labelEdad.AutoSize = true;
            this.labelEdad.Location = new System.Drawing.Point(32, 30);
            this.labelEdad.Name = "labelEdad";
            this.labelEdad.Size = new System.Drawing.Size(32, 13);
            this.labelEdad.TabIndex = 0;
            this.labelEdad.Text = "Edad";
            this.labelEdad.Click += new System.EventHandler(this.labelEdad_Click);
            // 
            // textBoxEdad
            // 
            this.textBoxEdad.Location = new System.Drawing.Point(70, 27);
            this.textBoxEdad.Name = "textBoxEdad";
            this.textBoxEdad.Size = new System.Drawing.Size(100, 20);
            this.textBoxEdad.TabIndex = 1;
            this.textBoxEdad.TextChanged += new System.EventHandler(this.textBoxEdad_TextChanged);
            // 
            // labelCalculo
            // 
            this.labelCalculo.AutoSize = true;
            this.labelCalculo.Location = new System.Drawing.Point(32, 67);
            this.labelCalculo.Name = "labelCalculo";
            this.labelCalculo.Size = new System.Drawing.Size(42, 13);
            this.labelCalculo.TabIndex = 2;
            this.labelCalculo.Text = "Calculo";
            this.labelCalculo.Click += new System.EventHandler(this.labelCalculo_Click);
            // 
            // buttonCalcularEdad
            // 
            this.buttonCalcularEdad.Location = new System.Drawing.Point(35, 100);
            this.buttonCalcularEdad.Name = "buttonCalcularEdad";
            this.buttonCalcularEdad.Size = new System.Drawing.Size(100, 23);
            this.buttonCalcularEdad.TabIndex = 3;
            this.buttonCalcularEdad.Text = "Calcular edad";
            this.buttonCalcularEdad.UseVisualStyleBackColor = true;
            this.buttonCalcularEdad.Click += new System.EventHandler(this.buttonCalcularEdad_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.buttonCalcularEdad);
            this.Controls.Add(this.labelCalculo);
            this.Controls.Add(this.textBoxEdad);
            this.Controls.Add(this.labelEdad);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEdad;
        private System.Windows.Forms.TextBox textBoxEdad;
        private System.Windows.Forms.Label labelCalculo;
        private System.Windows.Forms.Button buttonCalcularEdad;
    }
}

