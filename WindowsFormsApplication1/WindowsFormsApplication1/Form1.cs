﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void labelEdad_Click(object sender, EventArgs e)
        {

        }

        private void textBoxEdad_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelCalculo_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonCalcularEdad_Click(object sender, EventArgs e)
        {
            string Edad;
            int Valor;

            Edad = textBoxEdad.Text;

            // Combertir un String a int
            Valor = Int32.Parse(Edad);

            if ((Valor >= 18) && (Valor < 26))
                labelCalculo.Text = "La edad es: " + Edad + ", es mayor de edad y es joven";
            else if ((Valor >= 18) && (Valor > 26))
                labelCalculo.Text = "La edad es: " + Edad + ", es mayor de edad y no es joven";
            else
                labelCalculo.Text = "La edad es: " + Edad + ", es menor de edad";


        }
    }
}
