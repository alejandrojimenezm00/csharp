﻿namespace _04___WindowsFormsExcepciones
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNumero1 = new System.Windows.Forms.TextBox();
            this.textBoxNumero2 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonSumar = new System.Windows.Forms.Button();
            this.textBoxDia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelResultadoDia = new System.Windows.Forms.Label();
            this.buttonDia = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(96, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero 2";
            // 
            // textBoxNumero1
            // 
            this.textBoxNumero1.Location = new System.Drawing.Point(174, 47);
            this.textBoxNumero1.Name = "textBoxNumero1";
            this.textBoxNumero1.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumero1.TabIndex = 2;
            // 
            // textBoxNumero2
            // 
            this.textBoxNumero2.Location = new System.Drawing.Point(174, 71);
            this.textBoxNumero2.Name = "textBoxNumero2";
            this.textBoxNumero2.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumero2.TabIndex = 3;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(91, 133);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(58, 13);
            this.labelResultado.TabIndex = 4;
            this.labelResultado.Text = "Resultado:";
            this.labelResultado.Click += new System.EventHandler(this.labelResultado_Click);
            // 
            // buttonSumar
            // 
            this.buttonSumar.Location = new System.Drawing.Point(174, 97);
            this.buttonSumar.Name = "buttonSumar";
            this.buttonSumar.Size = new System.Drawing.Size(75, 23);
            this.buttonSumar.TabIndex = 5;
            this.buttonSumar.Text = "Sumar";
            this.buttonSumar.UseVisualStyleBackColor = true;
            this.buttonSumar.Click += new System.EventHandler(this.buttonSumar_Click);
            // 
            // textBoxDia
            // 
            this.textBoxDia.Location = new System.Drawing.Point(174, 193);
            this.textBoxDia.Name = "textBoxDia";
            this.textBoxDia.Size = new System.Drawing.Size(100, 20);
            this.textBoxDia.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Dia de la semana";
            // 
            // labelResultadoDia
            // 
            this.labelResultadoDia.AutoSize = true;
            this.labelResultadoDia.Location = new System.Drawing.Point(99, 222);
            this.labelResultadoDia.Name = "labelResultadoDia";
            this.labelResultadoDia.Size = new System.Drawing.Size(31, 13);
            this.labelResultadoDia.TabIndex = 8;
            this.labelResultadoDia.Text = "DIA: ";
            // 
            // buttonDia
            // 
            this.buttonDia.Location = new System.Drawing.Point(174, 264);
            this.buttonDia.Name = "buttonDia";
            this.buttonDia.Size = new System.Drawing.Size(100, 23);
            this.buttonDia.TabIndex = 9;
            this.buttonDia.Text = "Dia de la semana";
            this.buttonDia.UseVisualStyleBackColor = true;
            this.buttonDia.Click += new System.EventHandler(this.buttonDia_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 311);
            this.Controls.Add(this.buttonDia);
            this.Controls.Add(this.labelResultadoDia);
            this.Controls.Add(this.textBoxDia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonSumar);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxNumero2);
            this.Controls.Add(this.textBoxNumero1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNumero1;
        private System.Windows.Forms.TextBox textBoxNumero2;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonSumar;
        private System.Windows.Forms.TextBox textBoxDia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelResultadoDia;
        private System.Windows.Forms.Button buttonDia;
    }
}

