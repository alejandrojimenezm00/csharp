﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04___WindowsFormsExcepciones
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public String nombreDiaSemana (int dia)
        {
            switch (dia)
            {
                case 1: return "lunes";
                case 2: return "Martes";
                case 3: return "Miercoles";
                case 4: return "jueves";
                case 5: return "viernes";
                case 6: return "sabado";
                case 7: return "domingo";
                case -1: throw new System.FormatException("Valor no permitido (-1)");
                default: throw new System.ArgumentOutOfRangeException("Valor fuera de rango (1-7)");
            }
        }

        private void buttonSumar_Click(object sender, EventArgs e)
        {
            try
            {
                int num1, num2, resultado;

                num1 = Int32.Parse(textBoxNumero1.Text);
                num2 = Int32.Parse(textBoxNumero2.Text);
               // num2 = Int32.Parse(null); // iniciacion para forzar la SystemException

                resultado = num1 + num2;

                labelResultado.Text = "La suma es " + resultado;
            }
            catch(FormatException error)
            {
                labelResultado.Text = "Error al convertir " + error.Message;
            }
            catch(OverflowException error2)
            {
                labelResultado.Text = "Error por desvordamiento " + error2.Message;
            }
            catch(SystemException error3)
            {
                labelResultado.Text = "Error el distema " + error3.Message;
            }
        }

        private void labelResultado_Click(object sender, EventArgs e)
        {

        }

        private void buttonDia_Click(object sender, EventArgs e)
        {
            try
            {
                labelResultadoDia.Text = nombreDiaSemana(Int32.Parse(textBoxDia.Text));
            }
            catch (ArgumentOutOfRangeException error)
            {
                labelResultadoDia.Text = "Te has pasado con el número " + error;
            }
            catch (FormatException error)
            {
                labelResultadoDia.Text = "No pongas numeros negativos " + error;
            }
        }
    }
}
