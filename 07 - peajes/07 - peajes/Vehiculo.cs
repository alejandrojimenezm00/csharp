﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07___peajes
{
    class Vehiculo
    {
        private string tipoVehiculo;
        private int matricula, peajeVisitado;
        private int ocupantes;

        public Vehiculo(string tipo, int m, int o, int pv)
        {
            tipoVehiculo = tipo;
            matricula = m;
            ocupantes = o;
            peajeVisitado = pv;
        }

        public Vehiculo()
        {
            tipoVehiculo = "";
            matricula = 0;
            peajeVisitado = 0;
            ocupantes = 1;
        }

        public int sacarOcupantes()
        {
            return ocupantes;
        }

        public string sacarTipoVehiculo()
        {
            return tipoVehiculo;
        }

        public string mostrarDatos()
        {
            return "Estos son los datos del vehiculo, matricula: " + matricula + " Tipo de vehiculo: " + tipoVehiculo + " Peaje donde ha pasado: " + peajeVisitado + " Numero de ocupantes: " + ocupantes;
        }

    }
}
