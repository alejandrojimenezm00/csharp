﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07___peajes
{
    class Program
    {
        static void Main(string[] args)
        {

            

            int parada = 0;
            int posicionActual = 0;

            do
            {

                string tipoVehiculo;
                int matricula, ocupantes, numPeaje = 0;

                Console.WriteLine("1 -> Introducir datos vehiculo");
                Console.WriteLine("2 -> Mostrar datos (dinero recaudado)");
                Console.WriteLine("3 -> Mostrar datos (pasajeros recogidos");
                Console.WriteLine("4 -> Mostrar datos (Total vehiculos registrados");
                Console.WriteLine("5 -> Mostra datos (vehiculo con mas ocupantes");
                Console.WriteLine("6 -> Mostrar datos (primer camnio en pasar)");
                Console.WriteLine("7 -> Salir");
                Console.WriteLine("Opcion: ");

                parada = int.Parse(Console.ReadLine());
         

                switch (parada)
                {
                    case 1:
                        Console.WriteLine("Tipo de vehiculo:");
                        tipoVehiculo = Console.ReadLine();
                        Console.WriteLine("Matricula:");
                        matricula = int.Parse(Console.ReadLine());
                        Console.WriteLine("Ocupantes:");
                        ocupantes = int.Parse(Console.ReadLine());

                        Peaje.registroVehiculo(posicionActual, tipoVehiculo, matricula, ocupantes, numPeaje);

                        Peaje.contarDinero(tipoVehiculo);
                        Peaje.contarOcupantes(ocupantes);

                        posicionActual++;

                        if (numPeaje == 4)
                            numPeaje = 0;
                        else
                            numPeaje++;

                        break;

                    case 2:
                        int dineroRecaudado = Peaje.mostrarDinero();
                        Console.WriteLine("Se han recaudado: " + dineroRecaudado + " Euros");
                        break;

                    case 3:
                        int pasajerosRegistrados = Peaje.mostrarOcupantes();
                        Console.WriteLine("Se han registrado: " + pasajerosRegistrados + " pasajeros");
                        break;

                    case 4:
                        Console.WriteLine("Han pasado por la autopista: " + posicionActual + " vehiculos");
                        break;

                    case 5:
                        Peaje.mostrarMasOcupantes(posicionActual);
                        break;
                    case 6:
                        Peaje.camnionesPrimerPeaje(numPeaje, posicionActual);
                        break;
                }

            } while (parada != 7);

        }
    }
}
