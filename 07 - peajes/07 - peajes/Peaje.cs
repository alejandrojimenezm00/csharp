﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07___peajes
{
    class Peaje
    {

        static int dineroRecaudado = 0;
        static int ocupantesContados = 0;
        
        static Vehiculo[] tablaVehiculos = new Vehiculo[256];


        public static void registroVehiculo(int posicionActual, String tipoVehiculo, int matricula, int ocupantes, int numPeaje)
        {
            tablaVehiculos[posicionActual] = new Vehiculo(tipoVehiculo, matricula, ocupantes, numPeaje);
        }


        public static int mostrarDinero ()
        {
            return dineroRecaudado;
        }

        public static int mostrarOcupantes()
        {
            return ocupantesContados;
        }

        public static void contarDinero (String vehiculo)
        {
            switch (vehiculo)
            {
                case "camion":
                    dineroRecaudado += 20;
                    break;
                case "turismo":
                    dineroRecaudado += 15;
                    break;
                case "colectivo":
                    dineroRecaudado += 10;
                    break;
            }
        }


        public static void contarOcupantes (int ocupantes)
        {
            ocupantesContados += ocupantes;
        }

        public static void mostrarMasOcupantes(int numVehiculos)
        {

            Vehiculo maxOcupantes = new Vehiculo();


            try
            {
                for (int i = 0; i < numVehiculos; i++)
                    if (tablaVehiculos[numVehiculos].sacarOcupantes() > maxOcupantes.sacarOcupantes())
                        maxOcupantes = tablaVehiculos[numVehiculos];

                Console.WriteLine(Convert.ToString(maxOcupantes.mostrarDatos()));
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e);
            }

        }

        public static void camnionesPrimerPeaje (int numPeaje, int numVehiculos)
        {
            for (int i = 0; i < numVehiculos; i++)
                if (tablaVehiculos[i].sacarTipoVehiculo() == "camnion")
                    Console.WriteLine(Convert.ToString(tablaVehiculos[i].mostrarDatos()));
                
        }

    }
}
