﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _10___repaso_Examen
{
    public partial class Form1 : Form
    {

        private int numZapatos;
        responsable[] numZapato;

        public Form1()
        {
            InitializeComponent();
            numZapatos = 0;
            numZapato = new responsable[20];
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {

            string categotia = textBoxCategoria.Text;
            string material = textBoxMaterial.Text;
            int tamanio = int.Parse(textBoxTamanio.Text);
            string color = textBoxColor.Text;
            int cantidad = int.Parse(textBoxCantidad.Text);

            if (numZapatos == 20)
            {
                labelInfo.Text = "numero maximo de zapatos registrados";
            }

            numZapato[numZapatos] = new responsable(categotia, material, tamanio, color, cantidad);
            labelInfo.Text = numZapato[numZapatos].devolverHombreMujer();

        }

        private void buttonImprimir_Click(object sender, EventArgs e)
        {
            String infoHombreMujer = "";

            for (int i = 0; i < numZapatos; i++)
                labelInfo.Text = numZapato[i].devolverHombreMujer() + "\n";

        }
    }
}
