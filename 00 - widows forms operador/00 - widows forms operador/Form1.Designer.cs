﻿namespace _00___widows_forms_operador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEdad = new System.Windows.Forms.Label();
            this.labelResultado = new System.Windows.Forms.Label();
            this.textBoxEdad = new System.Windows.Forms.TextBox();
            this.textBoxResultado = new System.Windows.Forms.TextBox();
            this.buttonCalcularEdad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelEdad
            // 
            this.labelEdad.AutoSize = true;
            this.labelEdad.Location = new System.Drawing.Point(31, 28);
            this.labelEdad.Name = "labelEdad";
            this.labelEdad.Size = new System.Drawing.Size(39, 18);
            this.labelEdad.TabIndex = 0;
            this.labelEdad.Text = "Edad";
            this.labelEdad.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(31, 58);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(67, 18);
            this.labelResultado.TabIndex = 1;
            this.labelResultado.Text = "Resultado";
            // 
            // textBoxEdad
            // 
            this.textBoxEdad.Location = new System.Drawing.Point(104, 25);
            this.textBoxEdad.Name = "textBoxEdad";
            this.textBoxEdad.Size = new System.Drawing.Size(100, 26);
            this.textBoxEdad.TabIndex = 2;
            this.textBoxEdad.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxResultado
            // 
            this.textBoxResultado.Location = new System.Drawing.Point(104, 55);
            this.textBoxResultado.Name = "textBoxResultado";
            this.textBoxResultado.Size = new System.Drawing.Size(100, 26);
            this.textBoxResultado.TabIndex = 3;
            // 
            // buttonCalcularEdad
            // 
            this.buttonCalcularEdad.Location = new System.Drawing.Point(104, 109);
            this.buttonCalcularEdad.Name = "buttonCalcularEdad";
            this.buttonCalcularEdad.Size = new System.Drawing.Size(100, 23);
            this.buttonCalcularEdad.TabIndex = 4;
            this.buttonCalcularEdad.Text = "Caclular edad";
            this.buttonCalcularEdad.UseVisualStyleBackColor = true;
            this.buttonCalcularEdad.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 524);
            this.Controls.Add(this.buttonCalcularEdad);
            this.Controls.Add(this.textBoxResultado);
            this.Controls.Add(this.textBoxEdad);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.labelEdad);
            this.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEdad;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.TextBox textBoxEdad;
        private System.Windows.Forms.TextBox textBoxResultado;
        private System.Windows.Forms.Button buttonCalcularEdad;
    }
}

