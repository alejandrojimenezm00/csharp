﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01___WidowsForms_Iteradores
{
    public partial class Form1 : Form
    {

        private int numero1;
        private int numero2;
        private int numero3;

        public Form1()
        {
            InitializeComponent();
            numero1 = 100;
            numero2 = 200;
            numero3 = 300;
        }

        private void ResultadoForE_Click(object sender, EventArgs e)
        {

        }

        private void ResultadoForEach_Click(object sender, EventArgs e)
        {

        }

        private void buttonEscribirTabla_Click(object sender, EventArgs e)
        {
            int[] tabla = new int[5];

            tabla[0] = 12;
            tabla[1] = 30;
            tabla[2] = 20;
            tabla[3] = 19;
            tabla[4] = 20;


            // Escribir la tabla con un for
            
            ResultadoForE.Text = "";
            for (int i = 0; i < tabla.Length; i++)
                ResultadoForE.Text += tabla[i] + " - ";


            // Escribir la tabla con un foreach

            ResultadoForEach.Text = "";

            foreach (int i in tabla)
                ResultadoForEach.Text += i + " - ";

        }

        private void mostrarValorNumeros()
        {
            labelNumero1.Text = numero1.ToString();
            labelNumero2.Text = numero2.ToString();
            labelNumero3.Text = numero3.ToString();
        }

        public void mediaAritmeticaEntrada(int n1, int n2, int n3)
        {
            n3 = (n1 + n2) / 2;
        }

        public void mediaAritmeticaEntradaSalida(int n1, int n2, ref int n3)
        {
            n3 = (n1 + n2) / 2;
        }

        public void mediaAritmeticaSalida(int n1, int n2, out int n3)
        {
            n3 = (n1 + n2) / 2;
        }

        private void buttonEntrada_Click(object sender, EventArgs e)
        {

            mediaAritmeticaEntrada(numero1, numero2, numero3);
            mostrarValorNumeros();

        }

        private void buttonMostrarDatos_Click(object sender, EventArgs e)
        {
            mostrarValorNumeros();
        }

        private void buttonSalida_Click(object sender, EventArgs e)
        {
            mediaAritmeticaSalida(numero1, numero2, out numero3);
            mostrarValorNumeros();
        }

        private void buttonEntradaSalida_Click(object sender, EventArgs e)
        {
            mediaAritmeticaEntradaSalida(numero1, numero2, ref numero3);
            mostrarValorNumeros();
        }
    }
}