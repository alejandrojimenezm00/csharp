﻿namespace _01___WidowsForms_Iteradores
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ResultadoForE = new System.Windows.Forms.Label();
            this.ResultadoForEach = new System.Windows.Forms.Label();
            this.buttonEscribirTabla = new System.Windows.Forms.Button();
            this.buttonEntrada = new System.Windows.Forms.Button();
            this.buttonSalida = new System.Windows.Forms.Button();
            this.buttonEntradaSalida = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelNumero3 = new System.Windows.Forms.Label();
            this.labelNumero2 = new System.Windows.Forms.Label();
            this.labelNumero1 = new System.Windows.Forms.Label();
            this.buttonMostrarDatos = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ResultadoForE
            // 
            this.ResultadoForE.AutoSize = true;
            this.ResultadoForE.Location = new System.Drawing.Point(22, 75);
            this.ResultadoForE.Name = "ResultadoForE";
            this.ResultadoForE.Size = new System.Drawing.Size(64, 13);
            this.ResultadoForE.TabIndex = 0;
            this.ResultadoForE.Text = "ResultdoFor";
            this.ResultadoForE.Click += new System.EventHandler(this.ResultadoForE_Click);
            // 
            // ResultadoForEach
            // 
            this.ResultadoForEach.AutoSize = true;
            this.ResultadoForEach.Location = new System.Drawing.Point(22, 97);
            this.ResultadoForEach.Name = "ResultadoForEach";
            this.ResultadoForEach.Size = new System.Drawing.Size(100, 13);
            this.ResultadoForEach.TabIndex = 1;
            this.ResultadoForEach.Text = "Resultado For each";
            this.ResultadoForEach.Click += new System.EventHandler(this.ResultadoForEach_Click);
            // 
            // buttonEscribirTabla
            // 
            this.buttonEscribirTabla.Location = new System.Drawing.Point(25, 37);
            this.buttonEscribirTabla.Name = "buttonEscribirTabla";
            this.buttonEscribirTabla.Size = new System.Drawing.Size(121, 23);
            this.buttonEscribirTabla.TabIndex = 2;
            this.buttonEscribirTabla.Text = "Boton escribir tabla";
            this.buttonEscribirTabla.UseVisualStyleBackColor = true;
            this.buttonEscribirTabla.Click += new System.EventHandler(this.buttonEscribirTabla_Click);
            // 
            // buttonEntrada
            // 
            this.buttonEntrada.Location = new System.Drawing.Point(25, 211);
            this.buttonEntrada.Name = "buttonEntrada";
            this.buttonEntrada.Size = new System.Drawing.Size(87, 23);
            this.buttonEntrada.TabIndex = 3;
            this.buttonEntrada.Text = "Entrada";
            this.buttonEntrada.UseVisualStyleBackColor = true;
            this.buttonEntrada.Click += new System.EventHandler(this.buttonEntrada_Click);
            // 
            // buttonSalida
            // 
            this.buttonSalida.Location = new System.Drawing.Point(25, 252);
            this.buttonSalida.Name = "buttonSalida";
            this.buttonSalida.Size = new System.Drawing.Size(87, 23);
            this.buttonSalida.TabIndex = 4;
            this.buttonSalida.Text = "Salida";
            this.buttonSalida.UseVisualStyleBackColor = true;
            this.buttonSalida.Click += new System.EventHandler(this.buttonSalida_Click);
            // 
            // buttonEntradaSalida
            // 
            this.buttonEntradaSalida.Location = new System.Drawing.Point(25, 290);
            this.buttonEntradaSalida.Name = "buttonEntradaSalida";
            this.buttonEntradaSalida.Size = new System.Drawing.Size(87, 23);
            this.buttonEntradaSalida.TabIndex = 5;
            this.buttonEntradaSalida.Text = "Entrada/Salida";
            this.buttonEntradaSalida.UseVisualStyleBackColor = true;
            this.buttonEntradaSalida.Click += new System.EventHandler(this.buttonEntradaSalida_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(154, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Numero 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(154, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Numero 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(154, 295);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Numero 3";
            // 
            // labelNumero3
            // 
            this.labelNumero3.AutoSize = true;
            this.labelNumero3.Location = new System.Drawing.Point(222, 295);
            this.labelNumero3.Name = "labelNumero3";
            this.labelNumero3.Size = new System.Drawing.Size(35, 13);
            this.labelNumero3.TabIndex = 11;
            this.labelNumero3.Text = "label4";
            // 
            // labelNumero2
            // 
            this.labelNumero2.AutoSize = true;
            this.labelNumero2.Location = new System.Drawing.Point(222, 262);
            this.labelNumero2.Name = "labelNumero2";
            this.labelNumero2.Size = new System.Drawing.Size(35, 13);
            this.labelNumero2.TabIndex = 10;
            this.labelNumero2.Text = "label5";
            // 
            // labelNumero1
            // 
            this.labelNumero1.AutoSize = true;
            this.labelNumero1.Location = new System.Drawing.Point(222, 221);
            this.labelNumero1.Name = "labelNumero1";
            this.labelNumero1.Size = new System.Drawing.Size(35, 13);
            this.labelNumero1.TabIndex = 9;
            this.labelNumero1.Text = "label6";
            // 
            // buttonMostrarDatos
            // 
            this.buttonMostrarDatos.Location = new System.Drawing.Point(157, 339);
            this.buttonMostrarDatos.Name = "buttonMostrarDatos";
            this.buttonMostrarDatos.Size = new System.Drawing.Size(75, 23);
            this.buttonMostrarDatos.TabIndex = 12;
            this.buttonMostrarDatos.Text = "Mostrar";
            this.buttonMostrarDatos.UseVisualStyleBackColor = true;
            this.buttonMostrarDatos.Click += new System.EventHandler(this.buttonMostrarDatos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 497);
            this.Controls.Add(this.buttonMostrarDatos);
            this.Controls.Add(this.labelNumero3);
            this.Controls.Add(this.labelNumero2);
            this.Controls.Add(this.labelNumero1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEntradaSalida);
            this.Controls.Add(this.buttonSalida);
            this.Controls.Add(this.buttonEntrada);
            this.Controls.Add(this.buttonEscribirTabla);
            this.Controls.Add(this.ResultadoForEach);
            this.Controls.Add(this.ResultadoForE);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ResultadoForE;
        private System.Windows.Forms.Label ResultadoForEach;
        private System.Windows.Forms.Button buttonEscribirTabla;
        private System.Windows.Forms.Button buttonEntrada;
        private System.Windows.Forms.Button buttonSalida;
        private System.Windows.Forms.Button buttonEntradaSalida;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelNumero3;
        private System.Windows.Forms.Label labelNumero2;
        private System.Windows.Forms.Label labelNumero1;
        private System.Windows.Forms.Button buttonMostrarDatos;
    }
}

