﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        public void index ()
        {
            try
            {
                labelPosicion.Text = "Tu cadena buscada empieza en la posición: " + textBoxIndex.Text.IndexOf(textBoxCadena.Text);
            }
            catch (ArgumentOutOfRangeException error)
            {
                textBoxCadena.Text = "Te has calentado " + error;
            }
            catch (ArgumentNullException error)
            {
                textBoxCadena.Text = "Pasame una cadena manin " + error;
            }
        }

        public void calcFecha()
        {
            String caracter = textBoxChar.Text;

            try
            {
               if (caracter != "#" || caracter != "&" || caracter != "/" || caracter != "-")
                    throw new System.FormatException("Caracter invalido");

                labelFechaCalculada.Text = textBoxDia.Text + caracter + textBoxMes.Text + caracter + textBoxAno.Text;
            }
            catch (FormatException error)
            {
                labelFechaCalculada.Text = "Prueba otra vez " + error;
            }


        }



        private void buttonCalcPosicion_Click(object sender, EventArgs e)
        {
            index();
           
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void buttonCalFecha_Click(object sender, EventArgs e)
        {
            calcFecha();
        }
    }
}
