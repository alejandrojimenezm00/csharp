﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalcPosicion = new System.Windows.Forms.Button();
            this.labelPosicion = new System.Windows.Forms.Label();
            this.textBoxIndex = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCadena = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelMes = new System.Windows.Forms.Label();
            this.labelAno = new System.Windows.Forms.Label();
            this.labelChar = new System.Windows.Forms.Label();
            this.labelDia = new System.Windows.Forms.Label();
            this.textBoxChar = new System.Windows.Forms.TextBox();
            this.textBoxDia = new System.Windows.Forms.TextBox();
            this.textBoxMes = new System.Windows.Forms.TextBox();
            this.textBoxAno = new System.Windows.Forms.TextBox();
            this.buttonCalFecha = new System.Windows.Forms.Button();
            this.labelFechaCalculada = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCalcPosicion
            // 
            this.buttonCalcPosicion.Location = new System.Drawing.Point(12, 97);
            this.buttonCalcPosicion.Name = "buttonCalcPosicion";
            this.buttonCalcPosicion.Size = new System.Drawing.Size(75, 23);
            this.buttonCalcPosicion.TabIndex = 0;
            this.buttonCalcPosicion.Text = "Posición";
            this.buttonCalcPosicion.UseVisualStyleBackColor = true;
            this.buttonCalcPosicion.Click += new System.EventHandler(this.buttonCalcPosicion_Click);
            // 
            // labelPosicion
            // 
            this.labelPosicion.AutoSize = true;
            this.labelPosicion.Location = new System.Drawing.Point(19, 135);
            this.labelPosicion.Name = "labelPosicion";
            this.labelPosicion.Size = new System.Drawing.Size(50, 13);
            this.labelPosicion.TabIndex = 1;
            this.labelPosicion.Text = "Posicion:";
            // 
            // textBoxIndex
            // 
            this.textBoxIndex.Location = new System.Drawing.Point(82, 40);
            this.textBoxIndex.Name = "textBoxIndex";
            this.textBoxIndex.Size = new System.Drawing.Size(100, 20);
            this.textBoxIndex.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ejercicio 1";
            // 
            // textBoxCadena
            // 
            this.textBoxCadena.Location = new System.Drawing.Point(82, 71);
            this.textBoxCadena.Name = "textBoxCadena";
            this.textBoxCadena.Size = new System.Drawing.Size(100, 20);
            this.textBoxCadena.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cadena";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Busqueda";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Ejercicio 2";
            // 
            // labelMes
            // 
            this.labelMes.AutoSize = true;
            this.labelMes.Location = new System.Drawing.Point(22, 254);
            this.labelMes.Name = "labelMes";
            this.labelMes.Size = new System.Drawing.Size(30, 13);
            this.labelMes.TabIndex = 8;
            this.labelMes.Text = "Mes:";
            // 
            // labelAno
            // 
            this.labelAno.AutoSize = true;
            this.labelAno.Location = new System.Drawing.Point(22, 284);
            this.labelAno.Name = "labelAno";
            this.labelAno.Size = new System.Drawing.Size(26, 13);
            this.labelAno.TabIndex = 9;
            this.labelAno.Text = "Año";
            // 
            // labelChar
            // 
            this.labelChar.AutoSize = true;
            this.labelChar.Location = new System.Drawing.Point(22, 313);
            this.labelChar.Name = "labelChar";
            this.labelChar.Size = new System.Drawing.Size(47, 13);
            this.labelChar.TabIndex = 10;
            this.labelChar.Text = "Caracter";
            // 
            // labelDia
            // 
            this.labelDia.AutoSize = true;
            this.labelDia.Location = new System.Drawing.Point(22, 229);
            this.labelDia.Name = "labelDia";
            this.labelDia.Size = new System.Drawing.Size(26, 13);
            this.labelDia.TabIndex = 11;
            this.labelDia.Text = "Dia:";
            // 
            // textBoxChar
            // 
            this.textBoxChar.Location = new System.Drawing.Point(82, 313);
            this.textBoxChar.Name = "textBoxChar";
            this.textBoxChar.Size = new System.Drawing.Size(100, 20);
            this.textBoxChar.TabIndex = 12;
            // 
            // textBoxDia
            // 
            this.textBoxDia.Location = new System.Drawing.Point(82, 229);
            this.textBoxDia.Name = "textBoxDia";
            this.textBoxDia.Size = new System.Drawing.Size(100, 20);
            this.textBoxDia.TabIndex = 13;
            // 
            // textBoxMes
            // 
            this.textBoxMes.Location = new System.Drawing.Point(82, 254);
            this.textBoxMes.Name = "textBoxMes";
            this.textBoxMes.Size = new System.Drawing.Size(100, 20);
            this.textBoxMes.TabIndex = 14;
            // 
            // textBoxAno
            // 
            this.textBoxAno.Location = new System.Drawing.Point(82, 284);
            this.textBoxAno.Name = "textBoxAno";
            this.textBoxAno.Size = new System.Drawing.Size(100, 20);
            this.textBoxAno.TabIndex = 15;
            // 
            // buttonCalFecha
            // 
            this.buttonCalFecha.Location = new System.Drawing.Point(25, 348);
            this.buttonCalFecha.Name = "buttonCalFecha";
            this.buttonCalFecha.Size = new System.Drawing.Size(75, 23);
            this.buttonCalFecha.TabIndex = 16;
            this.buttonCalFecha.Text = "Fecha";
            this.buttonCalFecha.UseVisualStyleBackColor = true;
            this.buttonCalFecha.Click += new System.EventHandler(this.buttonCalFecha_Click);
            // 
            // labelFechaCalculada
            // 
            this.labelFechaCalculada.AutoSize = true;
            this.labelFechaCalculada.Location = new System.Drawing.Point(147, 353);
            this.labelFechaCalculada.Name = "labelFechaCalculada";
            this.labelFechaCalculada.Size = new System.Drawing.Size(67, 13);
            this.labelFechaCalculada.TabIndex = 17;
            this.labelFechaCalculada.Text = "fecha dada: ";
            this.labelFechaCalculada.Click += new System.EventHandler(this.label5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 399);
            this.Controls.Add(this.labelFechaCalculada);
            this.Controls.Add(this.buttonCalFecha);
            this.Controls.Add(this.textBoxAno);
            this.Controls.Add(this.textBoxMes);
            this.Controls.Add(this.textBoxDia);
            this.Controls.Add(this.textBoxChar);
            this.Controls.Add(this.labelDia);
            this.Controls.Add(this.labelChar);
            this.Controls.Add(this.labelAno);
            this.Controls.Add(this.labelMes);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxCadena);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxIndex);
            this.Controls.Add(this.labelPosicion);
            this.Controls.Add(this.buttonCalcPosicion);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalcPosicion;
        private System.Windows.Forms.Label labelPosicion;
        private System.Windows.Forms.TextBox textBoxIndex;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCadena;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelMes;
        private System.Windows.Forms.Label labelAno;
        private System.Windows.Forms.Label labelChar;
        private System.Windows.Forms.Label labelDia;
        private System.Windows.Forms.TextBox textBoxChar;
        private System.Windows.Forms.TextBox textBoxDia;
        private System.Windows.Forms.TextBox textBoxMes;
        private System.Windows.Forms.TextBox textBoxAno;
        private System.Windows.Forms.Button buttonCalFecha;
        private System.Windows.Forms.Label labelFechaCalculada;
    }
}

